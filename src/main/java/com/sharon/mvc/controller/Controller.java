package com.sharon.mvc.controller;

import com.sharon.mvc.model.Model;
import com.sharon.mvc.view.View;

/**
 * Created by orensharon on 2/26/17.
 */
public class Controller {
    public void startApplication() {
        // View the application's GUI
        View view = new View();
        view.setVisible(true);
    }

    public String getMessage() {
        try {
            Model model = new Model();
            return model.getData();
        } catch (Exception er) {
            return "There was an error.";
        }
    }

    public boolean writeMessage(String message) {
        try {
            Model model = new Model();
            return model.writeData(message);
        } catch (Exception er) {
            return false;
        }
    }
}

