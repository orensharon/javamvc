package com.sharon.mvc;

import com.sharon.mvc.controller.Controller;

/**
 * Created by orensharon on 2/26/17.
 */
public class MvcApplication {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Controller controller = new Controller();
        // Start the application
        controller.startApplication();
    }

}
